package com.cpl_cursos.java.ejemplos.crud_simple.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name="usuarios")
public class Usuario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idusuario", nullable = false)
    private Long idUsuario;
    //@NotEmpty
    private String nombre;
    //@NotEmpty
    private String apellidos;
    private String alias;
    private String clave;
    //@NotEmpty
    @Email
    private String email;
    private String dir1;
    private String dir2;
    private String localidad;
    private String cp;
    private String provincia;
    private String tlf;
    @Column(name="creado_el", columnDefinition = "date DEFAULT (curdate())")
    //@NotNull
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate creadoEl = LocalDate.now();
    private static final Long serialVersionUID = 1L;

    public Usuario() {
    }

    public Usuario(String nombre, String apellidos, String email, LocalDate fecha) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.creadoEl = fecha;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public String getAlias() {
        return alias;
    }
    public void setAlias(String alias) {
        this.alias = alias;
    }
    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getDir1() {
        return dir1;
    }
    public void setDir1(String dir1) {
        this.dir1 = dir1;
    }
    public String getDir2() {
        return dir2;
    }
    public void setDir2(String dir2) {
        this.dir2 = dir2;
    }
    public String getLocalidad() {
        return localidad;
    }
    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }
    public String getCp() {
        return cp;
    }
    public void setCp(String cp) {
        this.cp = cp;
    }
    public String getProvincia() {
        return provincia;
    }
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
    public String getTlf() {
        return tlf;
    }
    public void setTlf(String tlf) {
        this.tlf = tlf;
    }
    public LocalDate getCreadoEl() {
        return creadoEl;
    }
    public void setCreadoEl(LocalDate creadoEl) {
        this.creadoEl = creadoEl;
    }
}
