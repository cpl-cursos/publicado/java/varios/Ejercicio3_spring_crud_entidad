package com.cpl_cursos.java.ejemplos.crud_simple.controllers;

import com.cpl_cursos.java.ejemplos.crud_simple.entity.Usuario;
import com.cpl_cursos.java.ejemplos.crud_simple.service.IfxUsuarioSrvc;
import com.cpl_cursos.java.ejemplos.crud_simple.validadores.UsuarioValidador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Controller
@SessionAttributes("usuario") //<-- es el nombre que se pasa a las vistas y que aparece también en el formulario
public class UsuarioController {

    @Autowired
    private IfxUsuarioSrvc usuarioSrvc;  //<-- Al haber implementado la clase Service, usamos esta

    // inyectamos el validador
    @Autowired
    private UsuarioValidador validador;

    /*
        Permite utilizar el validador de forma transparente sin tener que llamarlo desde el código
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        //binder.setValidator(validador); //<-- Hace que SOLO se utilice al validdor indicado. Por eso se pierden las anotaciones.
        binder.addValidators(validador); //<-- Para que no se pierdan las otras validaciones
    }

    @GetMapping("/listausuarios")
    public String listaTodos(Model modelo) {
        modelo.addAttribute("titulopest", "Usuario");
        modelo.addAttribute("titulo", "Listado de usuarios");
        List<Usuario> lista = usuarioSrvc.findAll();
        modelo.addAttribute("usuarios", lista);
        return "lista_usuarios";
    }

    @GetMapping("/usuario/{id}")
    public String editar(@PathVariable(name="id") Long idusu, Map<String, Object> modelo) {
        modelo.put("titulopest", "Perfil");
        modelo.put("titulo", "Perfil de usuario");
        Usuario usu;
        if(idusu > 0) {
            usu = usuarioSrvc.findOne(idusu);
        } else {
            return "redirect:/listausuarios";
        }
        modelo.put("usuario", usu);
        return "form_usuario";
    }

    @GetMapping("/nuevousuario")
    public String nuevo(Model modelo) {
        modelo.addAttribute("titulopest", "Nuevo Usuario");
        modelo.addAttribute("titulo", "Alta de Usuario");
        Usuario usuario= new Usuario();
        modelo.addAttribute("usuario", usuario);
        return "form_usuario";
    }

    @PostMapping("/nuevousuario")
    /*
        @Valid permite verificar los datos recibidos partiendo de las restricciones impuestas en la clase Usuario.
        BindinResult asigna los resultados de la validación a la variable resultado. Contiene los errores. DEBE ponerse
        DESPUES del objeto a validar
     */
    public String nuevo(@Valid Usuario usuario, BindingResult resultado, ModelMap modelo, SessionStatus estado) throws ParseException {
        /*
            Como hemos utilizado @InitBinder, ya no es necesario utilizarlo aquí.
         */
        // validamos el formulario
        //validador.validate(usuario, resultado);

        // forma manual de obtener los errores
        if (resultado.hasErrors()) {
            /*
                Puedo haber llegado aquí desde la ruta con un id de usuario o desde la ruta de nuevo usuario.
                Tengo que volver al mismo sitio en caso de error en el formulario.
             */
            if(usuario.getIdUsuario() != null && usuario.getIdUsuario() > 0){
                return "redirect:/usuario/".concat(usuario.getIdUsuario().toString());
            } else {
                return "redirect:/nuevousuario";
            }
        }

        modelo.addAttribute("titulopest", "Perfil");
        modelo.addAttribute("titulo", "Perfil de usuario");
        modelo.addAttribute("usuario",usuario);
        usuarioSrvc.save(usuario);
        estado.setComplete();
        return "resultado";
    }

    @GetMapping("/borrausuario/{id}")
    public String borrar(@PathVariable(name="id") Long idusu, Model modelo) {
        usuarioSrvc.delete(idusu);
        return "redirect:/listausuarios";
    }
}
