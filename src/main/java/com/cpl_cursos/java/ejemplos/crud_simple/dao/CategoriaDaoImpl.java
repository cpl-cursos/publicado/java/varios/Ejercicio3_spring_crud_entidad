package com.cpl_cursos.java.ejemplos.crud_simple.dao;

import com.cpl_cursos.java.ejemplos.crud_simple.entity.Categoria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("CategoriaDaoJPA")  //<-- indica que es un elemento de Spring
public class CategoriaDaoImpl implements IfxCategoria{

    @PersistenceContext
    private EntityManager em;

    @Transactional(readOnly = true)  //<-- por ser un proceso de solo lectura
    @Override
    public List<Categoria> findAll() {
        return em.createQuery("from Categoria").getResultList();
    }

    @Transactional(readOnly = true)
    @Override
    public Categoria findOne(Long id) {
        return em.find(Categoria.class, id);
    }

    @Transactional
    @Override
    public void save(Categoria categoria) {
        if(categoria.getIdCategoria() != null && categoria.getIdCategoria() > 0) {
            em.merge(categoria);
        } else {
            em.persist(categoria);
        }
    }

    @Transactional
    @Override
    public void delete(Long id) {
        em.remove(findOne(id));
    }
}
