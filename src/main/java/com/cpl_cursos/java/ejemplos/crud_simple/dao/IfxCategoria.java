package com.cpl_cursos.java.ejemplos.crud_simple.dao;

import com.cpl_cursos.java.ejemplos.crud_simple.entity.Categoria;
import com.cpl_cursos.java.ejemplos.crud_simple.entity.Usuario;

import java.util.List;

public interface IfxCategoria {
    public List<Categoria> findAll();
    public Categoria findOne(Long id);
    public void save(Categoria categoria);
    public void delete(Long id);
}
