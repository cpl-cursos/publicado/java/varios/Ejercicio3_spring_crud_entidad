package com.cpl_cursos.java.ejemplos.crud_simple.service;

import com.cpl_cursos.java.ejemplos.crud_simple.entity.Usuario;

import java.util.List;

public interface IfxUsuarioSrvc {
    public List<Usuario> findAll();
    public Usuario findOne(Long id);
    public void save(Usuario usuario);
    public void delete(Long id);
}
