package com.cpl_cursos.java.ejemplos.crud_simple.dao;

import com.cpl_cursos.java.ejemplos.crud_simple.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

// No hace falta anotarlo porque CrudRepository ya es componente Spring
public interface IfxUsuarioDao extends CrudRepository<Usuario, Long> {

}
