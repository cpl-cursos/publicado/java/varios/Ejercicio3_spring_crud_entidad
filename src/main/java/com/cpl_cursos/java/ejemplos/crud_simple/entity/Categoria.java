package com.cpl_cursos.java.ejemplos.crud_simple.entity;

import javax.persistence.*;

@Entity
@Table(name="categorias")
public class Categoria {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcategoria", nullable = false)
    private Long idCategoria;
    @Column(nullable = false)
    private String categoria;
    @Column(name="codcategoria", nullable = false)
    private String codCategoria;

    public Categoria() {
    }

    // No hace falta el id porque es autonumérico
    public Categoria(String categoria, String codCategoria) {
        this.categoria = categoria;
        this.codCategoria = codCategoria;
    }

    public Long getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getCodCategoria() {
        return codCategoria;
    }

    public void setCodCategoria(String codCategoria) {
        this.codCategoria = codCategoria;
    }
}
