package com.cpl_cursos.java.ejemplos.crud_simple.service;

import com.cpl_cursos.java.ejemplos.crud_simple.dao.IfxUsuarioDao;
import com.cpl_cursos.java.ejemplos.crud_simple.entity.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service //<-- Implementa el patron de diseño "fachada" (facade). Único punto de entrada a los dao. Evita acceder directamente a los dao
public class UsuarioSrvcImpl implements IfxUsuarioSrvc {
    /*
        Podríamos tener varios dao y operar con ellos en la misma clase service.
        También nos quita la necesidad de trabajar con las transacciones en los dao, por lo que se pueden pasar aquí.
        Dentro de un método se podría interactuar con diferentes dao dentro de la misma transacción.
     */
    @Autowired
    private IfxUsuarioDao usuarioDao;

    @Override
    @Transactional(readOnly = true)  //<-- por ser un proceso de solo lectura
    public List<Usuario> findAll() {
        return (List<Usuario>) usuarioDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Usuario findOne(Long id) {
        return usuarioDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void save(Usuario usuario) {
        usuarioDao.save(usuario);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        usuarioDao.deleteById(id);
    }
}
