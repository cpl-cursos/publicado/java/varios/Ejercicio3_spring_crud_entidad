PASOS para construir el proyecto
================================

1 – Crear Proyecto Spring.

        •Asignar parámetros del proyecto

        •Crear proyecto Maven (automatico desde IntelliJ)

2 – Crear la entidad.

3 – Creamos el DAO.

4 – Creamos el controlador para cada ruta.

5 – Creamos las plantillas utilizadas por el controlador.

6 – Definimos los parámetros de conexión a la BBDD en application.properties.
    (Realmente este paso se puede hacer en cualquier momento, siempre quese haga antes de ejecutar el proyecto)

7 – Crear import.sql dentro de /resources para cargar datos de prueba.